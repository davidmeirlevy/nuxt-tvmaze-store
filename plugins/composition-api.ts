import Vue from 'vue'
import {Store} from 'vuex'
import VueCompositionAPI from '@vue/composition-api'

Vue.use(VueCompositionAPI)

