export default function fetchProduct({store, route}: any) {
  return store.dispatch('fetchProduct', route.params.productId);
}
