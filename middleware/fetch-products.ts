export default function fetchProducts({store, route}: any) {
  return store.dispatch('fetchProducts', route.query.q);
}
