import {ActionTree, MutationTree} from 'vuex'
import axios from 'axios';

export const state = () => ({
  products: [],
  product: {}
})


export const actions: ActionTree<any, any> = {
  async fetchProducts({commit}, querySearch) {
    const products = (await axios.get(`http://api.tvmaze.com/search/shows?q=${encodeURIComponent(querySearch)}}`)).data
      .map((item: any) => item.show);

    commit('setProducts', products);
  },
  async fetchProduct({commit}, productId) {
    const product = (await axios.get(`http://api.tvmaze.com/shows/${productId}`)).data

    commit('setProduct', product);
  }
}

export const mutations: MutationTree<any> = {
  setProducts: (state: any, products: Array<any>) => state.products = products,
  setProduct: (state: any, product: any) => state.product = product
}
